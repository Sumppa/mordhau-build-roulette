import React from "react";
import "./App.css";
import GenerateRandom from "./components/generateRandom/index";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <GenerateRandom />
      </header>
    </div>
  );
}

export default App;
