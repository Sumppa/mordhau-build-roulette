import React from "react";
import * as jsonObj from "./weapons-armors-perks.json";
import "../../index.css";
const jsonWeapons = jsonObj["weapons"];
const jsonHeads = jsonObj["head"];
const jsonTorsos = jsonObj["torso"];
const jsonLegs = jsonObj["legs"];
const jsonPerks = jsonObj["perks"];

class GenerateRandom extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      weapons: ["---", "---", "---"],
      head: "no head",
      torso: "no torso",
      legs: "no legs",
      perks: ["---"],
      usedGold: 0
    };
  }
  render() {
    const generateRandoms = () => {
      //const maxGold = 16;
      this.setState({ usedGold: 0 });

      // generate random number of weapons
      const generateRandomWeapons = () => {
        this.setState({ weapons: [], head: " ", torso: " ", legs: " " });

        const maxWeaponValue = jsonWeapons.length;
        let numberOfWeapons = Math.floor(Math.random() * 3) + 1;

        for (let i = 0; i < numberOfWeapons; i++) {
          let randomWeaponValue = Math.floor(Math.random() * maxWeaponValue);
          const selectedWeapon = jsonWeapons[randomWeaponValue];
          this.setState(prevState => ({
            usedGold: prevState.usedGold + selectedWeapon.Price,
            weapons: [...prevState.weapons, selectedWeapon.Name]
          }));
        }
      };

      // generate head
      const generateRandomHead = () => {
        const maxHeadValue = jsonHeads.length;
        let randomHeadValue = Math.floor(Math.random() * maxHeadValue);

        const selectedHead = jsonHeads[randomHeadValue];
        this.setState(prevState => ({
          usedGold: prevState.usedGold + selectedHead.Price,
          head: [...prevState.head, selectedHead.Name]
        }));
      };

      // genrate torso
      const generateRandomTorso = () => {
        const maxTorsoValue = jsonTorsos.length;
        let randomTorsoValue = Math.floor(Math.random() * maxTorsoValue);

        const selectedTorso = jsonTorsos[randomTorsoValue];
        this.setState(prevState => ({
          usedGold: prevState.usedGold + selectedTorso.Price,
          torso: [...prevState.torso, selectedTorso.Name]
        }));
      };

      // generate legs
      const generateRandomLegs = () => {
        const maxLegValue = jsonLegs.length;
        let randomLegValue = Math.floor(Math.random() * maxLegValue);

        const selectedLegs = jsonLegs[randomLegValue];
        this.setState(prevState => ({
          usedGold: prevState.usedGold + selectedLegs.Price,
          legs: [...prevState.legs, selectedLegs.Name]
        }));
      };

      // generate perks
      const generateRandomPerks = () => {
        this.setState({ perks: [] });
        let tempPerks = jsonPerks;
        const maxPerkValue = tempPerks.length;
        let numberOfPerks = Math.floor(Math.random() * 5) + 1;
        let selectedPerks = [];
        for (let i = 0; i < numberOfPerks; i++) {
          let randomPerkValue = Math.floor(Math.random() * maxPerkValue);
          selectedPerks.push(tempPerks[randomPerkValue]);

          //selectedPerks.filter(perk => perk !== selectedPerks);
          // this.setState(prevState => ({
          //   usedGold: prevState.usedGold + selectedPerks.Price,
          //   perks: [...prevState.perks, selectedPerks.Name]
          // }));
        }
      };
      generateRandomWeapons();
      generateRandomHead();
      generateRandomTorso();
      generateRandomLegs();
      generateRandomPerks();
    };
    return (
      <div className="generateRandom-wrapper">
        <h1 className="cost-counter">Cost: {this.state.usedGold}</h1>
        <div className="element-wrapper">
          <div className="weapon-wrapper">
            <ul>
              {this.state.weapons.map((item, i) => (
                <li key={i + 1} className="weapon-list">
                  <h1>Weapon {i + 1}</h1>
                  <br />
                  {item}
                </li>
              ))}
            </ul>
          </div>
          <div className="armor-wrapper">
            <div className="wrapper">
              <h1>Head:</h1>
              <p>{this.state.head}</p>
            </div>

            <div className="wrapper">
              <h1>Torso:</h1>
              <p>{this.state.torso}</p>
            </div>

            <div className="wrapper">
              <h1>Legs:</h1>
              <p>{this.state.legs}</p>
            </div>
          </div>
          <div className="perks-wrapper">
            <ul>
              {this.state.perks.map((perk, i) => (
                <li key={i} className="weapon-list">
                  <h1>Perk {i + 1}</h1>
                  <br />
                  {perk}
                </li>
              ))}
            </ul>
          </div>
        </div>
        <button onClick={generateRandoms}>Get Random Build</button>
      </div>
    );
  }
}

export default GenerateRandom;
