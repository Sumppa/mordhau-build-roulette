This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

### `TODO`
- Make sure same perk can't be on the list twice
- Check prices and don't allow item/armor/perk generation above 16 points.